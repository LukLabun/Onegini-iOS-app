//
//  EarthquakeCatalogAPI.m
//  Onegini-iOS-app
//
//  Created by Lukasz on 01.07.2017.
//  Copyright © 2017 Łukasz Łabuński. All rights reserved.
//

#import "EarthquakeCatalogAPI.h"
#import "EarthquakeInfoMapping.h"
#import "EarthquakeInfo.h"
#import <RestKit.h>

@interface EarthquakeCatalogAPI()

@property(nonatomic) RKObjectManager* manager;

@end

@implementation EarthquakeCatalogAPI

const double MAXRADIUS_KM = 500;

-(id) init {
    self = [super self];
    if (self) {
        self.manager = [RKObjectManager managerWithBaseURL:[NSURL URLWithString: [EarthquakeCatalogAPI getBaseUrl]]];
        RKResponseDescriptor* earthquakeInfoDescriptor = [EarthquakeInfoMapping requestMapping];
        [self.manager addResponseDescriptor:earthquakeInfoDescriptor];
    }
    return self;
}

+(NSString*) getBaseUrl {
    return [[NSBundle mainBundle] objectForInfoDictionaryKey:@"Earthquake Base URL"];
}

-(void) getEarthquakesByLocalizationWithBlock: (float) longitude latitude: (float) latitude
                                        block: (void (^)(NSArray* earthquakeArray)) block {
    
    NSDictionary* params = @{@"format" : @"geojson",
                             @"longitude" : [NSNumber numberWithFloat: longitude],
                             @"latitude" :  [NSNumber numberWithFloat: latitude],
                             @"maxradiuskm" : [NSNumber numberWithDouble: MAXRADIUS_KM]
                             };
    

    [self.manager getObjectsAtPath:@"/fdsnws/event/1/query" parameters:params
                      success:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult) {
    
                          block(mappingResult.array);
    }
                      failure:^(RKObjectRequestOperation *operation, NSError *error) {
                          NSLog(@"%@", error.description);
    }];
    
    
}

@end
