//
//  TwitterAPI.m
//  Onegini-iOS-app
//
//  Created by Lukasz on 02.07.2017.
//  Copyright © 2017 Łukasz Łabuński. All rights reserved.
//

#import "TwitterAPI.h"
#import "TweetMapping.h"
#import "OAuthToken.h"
#import "OAuthTokenMapping.h"

@interface TwitterAPI()

@property(nonatomic) RKObjectManager* manager;

@end

@implementation TwitterAPI

const double RADIUS_KM = 500;

-(id) init {
    self = [super init];
    
    if (self) {
        self.manager = [RKObjectManager managerWithBaseURL:[NSURL URLWithString:@"https://api.twitter.com/"]];
        RKResponseDescriptor* oauthTokenDescriptor = [OAuthTokenMapping requestMapping];
        RKResponseDescriptor* tweetDescriptor = [TweetMapping requestMapping];
        [self.manager addResponseDescriptorsFromArray:@[oauthTokenDescriptor, tweetDescriptor]];
    }
    
    return self;
}


-(void) getOAuthTokenWithBlock: (void (^)(OAuthToken*))block {
    
    NSString* clientSecret = @"hAqCBkThne19lVBgtx6d1RSv5SmrG8CPU3ssOMOT6SE76fsaKw";
    NSString* clientID = @"O4vEb6sn9uuT1y4pTGFKRYEXA";
    NSString* base64Encoded =  [NSString stringWithFormat:@"%@:%@", clientID, clientSecret];
    NSData* encodedBase64 = [base64Encoded dataUsingEncoding:NSUTF8StringEncoding];
    NSString* string = [encodedBase64 base64EncodedStringWithOptions:0];
    
    [[self.manager HTTPClient] setDefaultHeader:@"Authorization" value: [NSString stringWithFormat:@"Basic %@", string]];
    [[self.manager HTTPClient] setDefaultHeader:@"Content-Type" value:@"application/x-www-form-urlencoded;charset=UTF-8"];
    [self.manager postObject:nil path:@"/oauth2/token" parameters:@{@"grant_type" : @"client_credentials"}
                success:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult) {
                    block(mappingResult.firstObject);
                }
                failure:^(RKObjectRequestOperation *operation, NSError *error) {
                    NSLog(@"%@", error.description);
    }];
    
}

-(void) getTweetsByLocationWithBlock:(float) longitude latitude: (float) latitude block: (void (^)(NSArray* earthquakeArray)) block {
    
    NSDictionary* params = @{@"q" : @"earthquake",
                             @"geocode" : [NSString stringWithFormat:@"%@,%@,%@km",
                                           [NSNumber numberWithFloat:longitude], [NSNumber numberWithFloat:latitude], [NSNumber numberWithDouble: RADIUS_KM]]
                             };
    
    [self getOAuthTokenWithBlock:^(OAuthToken* token) {
        
        [[self.manager HTTPClient] setDefaultHeader:@"Authorization" value: [NSString stringWithFormat:@"%@ %@", token.token_type, token.access_token]];
    
        [self.manager getObjectsAtPath:@"/1.1/search/tweets.json" parameters:params
                      success:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult) {
                          
                          block(mappingResult.array);
                          
                      }
                      failure:^(RKObjectRequestOperation *operation, NSError *error) {
                          NSLog(@"%@", error.description);
                      }];
    }];

}

@end
