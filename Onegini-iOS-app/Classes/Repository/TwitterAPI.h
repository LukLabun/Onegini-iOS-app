//
//  TwitterAPI.h
//  Onegini-iOS-app
//
//  Created by Lukasz on 02.07.2017.
//  Copyright © 2017 Łukasz Łabuński. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <RestKit/RestKit.h>
#import "OAuthToken.h"

@interface TwitterAPI : NSObject

-(void) getTweetsByLocationWithBlock:(float) longitude latitude: (float) latitude block: (void (^)(NSArray* earthquakeArray)) block;
-(void) getOAuthTokenWithBlock: (void (^)(OAuthToken*))block;

@end
