//
//  EarthquakeCatalogAPI.h
//  Onegini-iOS-app
//
//  Created by Lukasz on 01.07.2017.
//  Copyright © 2017 Łukasz Łabuński. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface EarthquakeCatalogAPI : NSObject

+(NSString*) getBaseUrl;

-(void) getEarthquakesByLocalizationWithBlock: (float) longitude latitude: (float) latitude
                                        block: (void (^)(NSArray* earthquakeArray)) block;

@end
