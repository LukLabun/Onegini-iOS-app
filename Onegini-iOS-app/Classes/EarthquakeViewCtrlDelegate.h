//
//  EarthquakeViewCtrlDelegate.h
//  Onegini-iOS-app
//
//  Created by Lukasz on 02.07.2017.
//  Copyright © 2017 Łukasz Łabuński. All rights reserved.
//

#ifndef EarthquakeViewCtrlDelegate_h
#define EarthquakeViewCtrlDelegate_h
#import "EarthquakeInfo.h"


#endif /* EarthquakeViewCtrlDelegate_h */

@protocol EarthquakeViewCtrlDelegate <NSObject>

-(void) getEarthquakesForCurrentLocationBlock:(void (^)(NSArray<EarthquakeInfo*> *))block;

@end

