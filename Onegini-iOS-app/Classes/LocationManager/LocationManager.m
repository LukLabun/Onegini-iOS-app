//
//  LocationManager.m
//  Onegini-iOS-app
//
//  Created by Lukasz on 02.07.2017.
//  Copyright © 2017 Łukasz Łabuński. All rights reserved.
//

#import "LocationManager.h"

@implementation LocationManager

CLLocationManager *locationManager;

-(id) init {
    self = [super init];
    
    if(self){
        locationManager = [[CLLocationManager alloc] init];
        locationManager.distanceFilter = kCLDistanceFilterNone;
        locationManager.desiredAccuracy = kCLLocationAccuracyHundredMeters;
        [locationManager startUpdatingLocation];
        [locationManager requestAlwaysAuthorization];
    }
    return self;
}

-(CLLocationCoordinate2D) getCoordinate {
    return locationManager.location.coordinate;
}


@end
