//
//  AppDelegate.m
//  Onegini-iOS-app
//
//  Created by Lukasz on 30.06.2017.
//  Copyright © 2017 Łukasz Łabuński. All rights reserved.
//

#import "AppDelegate.h"
#import "AppCoordinator.h"
#import "EarthquakeCatalogAPI.h"
#import "TwitterAPI.h"
#import <RestKit/RestKit.h>

@interface AppDelegate ()

@property(nonatomic) AppCoordinator* appCoordinator;

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    
//    RKLogConfigureByName("RestKit", RKLogLevelWarning);
//    RKLogConfigureByName("RestKit/ObjectMapping", RKLogLevelTrace);
//    RKLogConfigureByName("RestKit/Network", RKLogLevelTrace);
    
    UIWindow* window = [[[UIApplication sharedApplication] delegate] window];
    self.appCoordinator = [[AppCoordinator alloc] initWithWindow: window];
    
    [self.appCoordinator showRootViewCtrl];
     
    return YES;
}

@end
