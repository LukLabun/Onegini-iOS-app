//
//  AppDelegate.h
//  Onegini-iOS-app
//
//  Created by Lukasz on 30.06.2017.
//  Copyright © 2017 Łukasz Łabuński. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

