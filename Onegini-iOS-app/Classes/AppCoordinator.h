//
//  AppCoordinator.h
//  Onegini-iOS-app
//
//  Created by Lukasz on 02.07.2017.
//  Copyright © 2017 Łukasz Łabuński. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TwitterViewCtrlDelegate.h"
#import "EarthquakeViewCtrlDelegate.h"
#import <UIKit/UIKit.h>

@interface AppCoordinator : NSObject<TwitterViewCtrlDelegate, EarthquakeViewCtrlDelegate>

-(id) initWithWindow: (UIWindow*) window;
-(void) showRootViewCtrl;

@end
