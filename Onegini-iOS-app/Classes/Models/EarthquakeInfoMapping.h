//
//  EarthquakeInfoMapping.h
//  Onegini-iOS-app
//
//  Created by Lukasz on 01.07.2017.
//  Copyright © 2017 Łukasz Łabuński. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <RestKit/RestKit.h>

@interface EarthquakeInfoMapping : NSObject

+(RKResponseDescriptor* ) requestMapping;

@end
