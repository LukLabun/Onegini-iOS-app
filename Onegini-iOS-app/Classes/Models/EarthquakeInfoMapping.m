//
//  EarthquakeInfoMapping.m
//  Onegini-iOS-app
//
//  Created by Lukasz on 01.07.2017.
//  Copyright © 2017 Łukasz Łabuński. All rights reserved.
//

#import "EarthquakeInfoMapping.h"
#import "EarthquakeInfo.h"

@implementation EarthquakeInfoMapping

+(RKResponseDescriptor* ) requestMapping {
    RKObjectMapping* earthQuakeInfoMapping = [RKObjectMapping mappingForClass: [EarthquakeInfo class]];
    [earthQuakeInfoMapping addAttributeMappingsFromDictionary: @{@"mag" : @"mag",
                                                                 @"place" : @"place",
                                                                 @"time" : @"time"
                                                                  }];
    
    
    RKResponseDescriptor *responseDescriptor = [RKResponseDescriptor responseDescriptorWithMapping:earthQuakeInfoMapping
                                                                                            method:RKRequestMethodAny
                                                                                       pathPattern:nil
                                                                                           keyPath:@"features.properties"
                                                                                       statusCodes:RKStatusCodeIndexSetForClass(RKStatusCodeClassSuccessful)];

    return responseDescriptor;
}

@end
