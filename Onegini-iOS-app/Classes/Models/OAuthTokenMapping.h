//
//  OAuthTokenMapping.h
//  Onegini-iOS-app
//
//  Created by Lukasz on 02.07.2017.
//  Copyright © 2017 Łukasz Łabuński. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <RestKit/RestKit.h>

@interface OAuthTokenMapping : NSObject

+(RKResponseDescriptor* ) requestMapping;

@end
