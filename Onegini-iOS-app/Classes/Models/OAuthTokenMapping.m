//
//  OAuthTokenMapping.m
//  Onegini-iOS-app
//
//  Created by Lukasz on 02.07.2017.
//  Copyright © 2017 Łukasz Łabuński. All rights reserved.
//

#import "OAuthTokenMapping.h"
#import "OAuthToken.h"

@implementation OAuthTokenMapping

+(RKResponseDescriptor* ) requestMapping {
    RKObjectMapping* earthQuakeInfoMapping = [RKObjectMapping mappingForClass: [OAuthToken class]];
    [earthQuakeInfoMapping addAttributeMappingsFromDictionary: @{@"token_type" : @"token_type",
                                                                 @"access_token" : @"access_token"
                                                                 }];
    
    
    RKResponseDescriptor *responseDescriptor = [RKResponseDescriptor responseDescriptorWithMapping:earthQuakeInfoMapping
                                                                                            method:RKRequestMethodAny
                                                                                       pathPattern:nil
                                                                                           keyPath:nil
                                                                                       statusCodes:RKStatusCodeIndexSetForClass(RKStatusCodeClassSuccessful)];
    
    return responseDescriptor;
}


@end
