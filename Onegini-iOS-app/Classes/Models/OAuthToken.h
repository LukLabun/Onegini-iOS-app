//
//  OAuthToken.h
//  Onegini-iOS-app
//
//  Created by Lukasz on 02.07.2017.
//  Copyright © 2017 Łukasz Łabuński. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface OAuthToken : NSObject

@property(nonatomic) NSString *token_type;
@property(nonatomic) NSString *access_token;

@end
