//
//  TweetMapping.m
//  Onegini-iOS-app
//
//  Created by Lukasz on 02.07.2017.
//  Copyright © 2017 Łukasz Łabuński. All rights reserved.
//

#import "TweetMapping.h"
#import "Tweet.h"

@implementation TweetMapping

+(RKResponseDescriptor* ) requestMapping {
    RKObjectMapping* earthQuakeInfoMapping = [RKObjectMapping mappingForClass: [Tweet class]];
    [earthQuakeInfoMapping addAttributeMappingsFromDictionary: @{@"text" : @"text",
                                                                 }];
    
    
    RKResponseDescriptor *responseDescriptor = [RKResponseDescriptor responseDescriptorWithMapping:earthQuakeInfoMapping
                                                                                            method:RKRequestMethodAny
                                                                                       pathPattern:nil
                                                                                           keyPath:@"statuses"
                                                                                       statusCodes:RKStatusCodeIndexSetForClass(RKStatusCodeClassSuccessful)];
    
    return responseDescriptor;
}

@end
