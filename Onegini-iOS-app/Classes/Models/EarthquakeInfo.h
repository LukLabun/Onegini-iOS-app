//
//  EarthquakeInfo.h
//  Onegini-iOS-app
//
//  Created by Lukasz on 30.06.2017.
//  Copyright © 2017 Łukasz Łabuński. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface EarthquakeInfo : NSObject 

@property(nonatomic) NSString *place;
@property(nonatomic) NSDate *time;
@property(nonatomic) NSNumber *mag;

@end
