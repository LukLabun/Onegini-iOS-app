//
//  AppCoordinator.m
//  Onegini-iOS-app
//
//  Created by Lukasz on 02.07.2017.
//  Copyright © 2017 Łukasz Łabuński. All rights reserved.
//

#import "AppCoordinator.h"
#import "TwitterAPI.h"
#import "EarthquakeCatalogAPI.h"
#import "LocationManager.h"
#import "EarthquakeVC.h"
#import "TwitterVC.h"
#import "EarthquakeInfo.h"
#import <UIKit/UIKit.h>

@interface AppCoordinator()

@property(nonatomic) TwitterAPI* twitterApi;
@property(nonatomic) EarthquakeCatalogAPI* earthquakeApi;
@property(nonatomic) LocationManager* locationManager;
@property(nonatomic) UIWindow* window;
@property(nonatomic) UITabBarController* tabBarController;

@end

@implementation AppCoordinator

-(id) initWithWindow: (UIWindow*) window{
    self = [super init];
    
    if(self){
        self.window = window;
        self.tabBarController = [[UITabBarController alloc] init];
        self.window.rootViewController = self.tabBarController;
        self.twitterApi = [[TwitterAPI alloc] init];
        self.earthquakeApi = [[EarthquakeCatalogAPI alloc] init];
        self.locationManager = [[LocationManager alloc] init];
    }
    
    return self;
}

-(void) getTweetsForCurrentLocationBlock:(void (^)(NSArray<Tweet*> *))block {
    CLLocationCoordinate2D coordinate = [self.locationManager getCoordinate];
    
    [self.twitterApi getTweetsByLocationWithBlock: coordinate.longitude latitude: coordinate.latitude block:^(NSArray *earthquakeArray) {
        block(earthquakeArray);
    }];
}

-(void) getEarthquakesForCurrentLocationBlock:(void (^)(NSArray<EarthquakeInfo*> *))block {
    CLLocationCoordinate2D coordinate = [self.locationManager getCoordinate];
    
    [self.earthquakeApi getEarthquakesByLocalizationWithBlock:coordinate.longitude latitude:coordinate.latitude block:^(NSArray *earthquakeArray) {
        block(earthquakeArray);
    }];
}

-(void) showRootViewCtrl {
    UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    EarthquakeVC* earthquakeVC = [storyboard instantiateViewControllerWithIdentifier:@"earthquakeVC"];
    TwitterVC* twitterVC = [storyboard instantiateViewControllerWithIdentifier:@"twitterVC"];
    
    earthquakeVC.delegate = self;
    twitterVC.delegate = self;
    
    NSArray * viewControllers = [NSArray arrayWithObjects:earthquakeVC, twitterVC, nil];
    self.tabBarController.viewControllers = viewControllers;
    
    earthquakeVC.tabBarItem =  [[UITabBarItem alloc] initWithTitle:@"Earthquakes"
                                                             image:nil
                                                               tag:1];
    
    twitterVC.tabBarItem =  [[UITabBarItem alloc] initWithTitle:@"Tweets"
                                                             image:nil
                                                               tag:2];


}

@end
