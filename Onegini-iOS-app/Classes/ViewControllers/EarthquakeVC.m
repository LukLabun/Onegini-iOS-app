//
//  EarthquakeVC.m
//  Onegini-iOS-app
//
//  Created by Lukasz on 03.07.2017.
//  Copyright © 2017 Łukasz Łabuński. All rights reserved.
//

#import "EarthquakeVC.h"
#import "EarthquakeInfo.h"
#import "EarthquakeTableViewCell.h"

@interface EarthquakeVC ()
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (nonatomic) NSArray<EarthquakeInfo*> *earthquakeArray;

@end

@implementation EarthquakeVC

static NSString* earthquakeIdentifier = @"earthquakeCell";

- (void)viewDidLoad {
    [super viewDidLoad];
    self.tableView.dataSource = self;
    UINib *cellNib = [UINib nibWithNibName:@"EarthquakeTableViewCell" bundle:nil];
    [self.tableView registerNib:cellNib forCellReuseIdentifier: earthquakeIdentifier];
    
}

-(void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [self.delegate getEarthquakesForCurrentLocationBlock:^(NSArray<EarthquakeInfo*> *earthquakeArray) {
        self.earthquakeArray = earthquakeArray;
        [self.tableView reloadData];
    }];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    EarthquakeTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:earthquakeIdentifier];
    cell.locationLabel.text = [self.earthquakeArray objectAtIndex:indexPath.row].place;
    
    NSString *magString = [[self.earthquakeArray objectAtIndex:indexPath.row].mag stringValue];
    cell.magLabel.text = magString;
    
    NSDate *date = [self.earthquakeArray objectAtIndex:indexPath.row].time;
    NSString *dateString = [NSDateFormatter localizedStringFromDate:date
                                                          dateStyle:NSDateFormatterShortStyle
                                                          timeStyle:NSDateFormatterFullStyle];
    cell.timeLabel.text = dateString;

    return cell;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.earthquakeArray count];
}


@end
