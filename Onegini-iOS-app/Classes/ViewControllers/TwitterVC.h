//
//  TwitterVC.h
//  Onegini-iOS-app
//
//  Created by Lukasz on 03.07.2017.
//  Copyright © 2017 Łukasz Łabuński. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TwitterViewCtrlDelegate.h"

@interface TwitterVC : UIViewController<UITableViewDataSource>

@property(weak, nonatomic) id <TwitterViewCtrlDelegate> delegate;

@end
