//
//  EarthquakeTableViewCell.m
//  Onegini-iOS-app
//
//  Created by Lukasz on 03.07.2017.
//  Copyright © 2017 Łukasz Łabuński. All rights reserved.
//

#import "EarthquakeTableViewCell.h"

@implementation EarthquakeTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
