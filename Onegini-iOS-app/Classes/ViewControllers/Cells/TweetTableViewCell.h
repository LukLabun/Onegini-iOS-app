//
//  TweetTableViewCell.h
//  Onegini-iOS-app
//
//  Created by Lukasz on 03.07.2017.
//  Copyright © 2017 Łukasz Łabuński. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TweetTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *textLabel;

@end
