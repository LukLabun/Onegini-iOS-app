//
//  TwitterVC.m
//  Onegini-iOS-app
//
//  Created by Lukasz on 03.07.2017.
//  Copyright © 2017 Łukasz Łabuński. All rights reserved.
//

#import "TwitterVC.h"
#import "Tweet.h"
#import "TweetTableViewCell.h"

@interface TwitterVC ()
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (nonatomic) NSArray<Tweet*> *tweetArray;

@end

@implementation TwitterVC

static NSString* twitterIdentifier = @"twitterCell";

- (void)viewDidLoad {
    [super viewDidLoad];
    self.tableView.dataSource = self;
    UINib *cellNib = [UINib nibWithNibName:@"TweetTableViewCell" bundle:nil];
    [self.tableView registerNib:cellNib forCellReuseIdentifier: twitterIdentifier];
}

-(void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [self.delegate getTweetsForCurrentLocationBlock:^(NSArray<Tweet*> *tweetArray) {
        self.tweetArray = tweetArray;
        [self.tableView reloadData];
    }];
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.tweetArray count];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    TweetTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:twitterIdentifier];
    cell.textLabel.text = [self.tweetArray objectAtIndex:indexPath.row].text;
    
    return cell;
}



@end
