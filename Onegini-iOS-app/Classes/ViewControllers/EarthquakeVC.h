//
//  EarthquakeVC.h
//  Onegini-iOS-app
//
//  Created by Lukasz on 03.07.2017.
//  Copyright © 2017 Łukasz Łabuński. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EarthquakeViewCtrlDelegate.h"

@interface EarthquakeVC : UIViewController<UITableViewDataSource>

@property (weak, nonatomic) id<EarthquakeViewCtrlDelegate> delegate;

@end
