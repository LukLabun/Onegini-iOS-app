//
//  TwitterViewCtrlDelegate.h
//  Onegini-iOS-app
//
//  Created by Lukasz on 02.07.2017.
//  Copyright © 2017 Łukasz Łabuński. All rights reserved.
//

#ifndef TwitterViewCtrlDelegate_h
#define TwitterViewCtrlDelegate_h
#import "Tweet.h"


#endif /* TwitterViewCtrlDelegate_h */

@protocol TwitterViewCtrlDelegate <NSObject>

-(void) getTweetsForCurrentLocationBlock:(void (^)(NSArray<Tweet*> *))block;

@end
